# Flectra Community / partner-contact-extra

Flectra Partner and Contact related extra addons



Available addons
----------------

addon | version | summary
--- |---------| ---
[partner_copy_address](partner_copy_address) | 2.0     | Copy partners address to clipboard


