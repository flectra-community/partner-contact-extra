# -*- coding: utf-8 -*-
from flectra import models, fields


class ResPartner(models.Model):
    """ Inherit res.partner"""
    _inherit = 'res.partner'

    copy_add = fields.Text(string='Copy Address')

    def copy_address(self):
        address = ''

        if self.name:
            address += self.name
        if self.street:
            address += '\n' + self.street
        if self.street2:
            address += '\n' + self.street2
        if self.zip:
            address += '\n' + self.zip
        if self.city:
            address += ' ' + self.city
        if self.country_id:
            address += '\n' + self.country_id.name

        self.copy_add = address
