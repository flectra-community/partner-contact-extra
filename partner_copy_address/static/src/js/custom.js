flectra.define('partner.copy_address', function (require) {
'use strict';

var core = require('web.core');
var field_registry = require('web.field_registry');

var config = require('web.config');
var FormView = require('web.FormView');
var FormController = require('web.FormController');
var FormRenderer = require('web.FormRenderer');
var view_registry = require('web.view_registry');

var _t = core._t;

var CopyAddressRenderer = FormRenderer.extend({

    events: _.extend({}, FormRenderer.prototype.events, {
        'click .o_button_icon': function(e) {
            this._copy_add();
        },
        'click .o_stat_info': function(e) {
            this._copy_add();
        },
        'click .oe_stat_button': function(e) {
            this._copy_add();
        },
    }),

    _copy_add: function(){
        var address_partner = '';
        if(this.state.data.title)
        {
            address_partner = this.state.data.title.data['display_name']+'\n'
        }
        if(this.state.data.firstname)
        {
            address_partner += this.state.data.firstname+' '+this.state.data.lastname;
        }
        else
        {
            address_partner += this.state.data.name;
        }
        if (this.state.data.street)
        {
            address_partner += '\n' + this.state.data.street;
        }
        if (this.state.data.street2)
        {
            address_partner += '\n' + this.state.data.street2;
        }
        if (this.state.data.zip)
        {
            address_partner += '\n' + this.state.data.zip;
            if (this.state.data.city)
            {
                address_partner += ' ' + this.state.data.city;
            }
        }
        else if(this.state.data.city)
        {
            address_partner += this.state.data.city;
        }
        if (this.state.data.country_id)
        {
            address_partner += '\n' + this.state.data.country_id.data['display_name'];
        }
        navigator.clipboard.writeText(address_partner);
    },
});

var WidgetCopyAddress = FormView.extend({
    config: _.extend({}, FormView.prototype.config, {
        Renderer: CopyAddressRenderer,
    }),

    getRenderer: function (parent, state) {
        return new CopyAddressRenderer(parent, state, this.rendererParams);
    }
});

view_registry.add('copy_address', WidgetCopyAddress);
return {
    Renderer: CopyAddressRenderer,
};
});